import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import React, { Component } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { numberWithCommas } from '../utils/utils';
import {API_URL} from '../utils/constant'
import swal from 'sweetalert'
// import {Link} from 'react-router-dom'


export default class TotalBayar extends Component {
    
    submitTotalBayar = (totalBayar) => {
        const pesanan = {
            total_bayar : totalBayar,
            menus : this.props.keranjangs
        }
        const total = totalBayar
        
        if (total === 0) {
          swal({
            title: "Error",
            text: "Pilih Produk Yang Mau Di Beli",
            icon: "error",
            button: false,
            timer: 2000
          });
        }else{
          axios
          .post(API_URL+"pesanans",pesanan)
          .then((res) => {
            console.log(this.props.keranjangs);
              window.location.href='sukses'
          })
        }
       
    }
  render() {
    const totalBayar = this.props.keranjangs.reduce(function (result, item) {
        return result + item.total_harga;
      }, 0);
    return (
      <>
      {/* web */}
       <div className='fixed-bottom d-none d-md-block'>
        <Row>
            <Col md={{ span : 3,offset :9 }} className="px-4">
                <h4>Harga :<strong className='float-end me-2' >Rp.{numberWithCommas(totalBayar)}</strong> </h4>
                <div className="d-grid gap-2">
                    <Button variant='primary' size="lg" className='m-2' onClick={() => this.submitTotalBayar(totalBayar)}  >
                        <FontAwesomeIcon icon={faShoppingCart} /><strong>Bayar</strong>
                    </Button>
                </div>
            </Col>
        </Row>
      </div>
      {/* mobile */}
      <div className='d-sm-block d-md-none '>
        <Row>
            <Col md={{ span : 3,offset :9 }} className="px-4">
                <h4>Harga :<strong className='float-end me-2' >Rp.{numberWithCommas(totalBayar)}</strong> </h4>
                <div className="d-grid gap-2">
                    <Button variant='primary' size="lg" className='m-2' onClick={() => this.submitTotalBayar(totalBayar)}  >
                        <FontAwesomeIcon icon={faShoppingCart} /><strong>Bayar</strong>
                    </Button>
                </div>
            </Col>
        </Row>
      </div>
      </>
     
    )
  }
}
